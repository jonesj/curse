import sqlite3

database = sqlite3.connect("curse.db")

cursor = database.cursor()


class courseroster:
    roster = []

    def addstudent(self, name):
        self.roster.append(name)

    def removestudent(self, name):
        for i in range(len(self.roster)):
            if self.roster[i] == name:
                del self.roster[i]

    def printroster(self):
        for i in range(len(self.roster)):
            print(self.roster[i])


Digital_Logic = courseroster()
Differential_Equations = courseroster()
Circuits = courseroster()
English = courseroster()
Digital_Circuit_Design = courseroster()
Nanotechnoloy = courseroster()


def instructor():
    k = 1
    while k:
        m = 1
        n = 1
        ins = input("Welcome Instructor. What would you like to do?\n 0: View class roster\n 1: Logout\n")
        if ins == '0':
            while m:
                tempname = input("Enter your last name to search for your class\n")
                cursor.execute("SELECT SURNAME FROM INSTRUCTOR WHERE SURNAME = ?", (tempname,))
                results = cursor.fetchall()
                if len(results) == 0:
                    print("You entered your name incorrectly, please try again.\n")
                    m = 1
                else:
                    cursor.execute("SELECT CRN, COURSE_NAME FROM COURSES WHERE PROFESSOR = ?", (tempname,))
                    results = cursor.fetchall()
                    if len(results) == 0:
                        print("You do not currently have any classes assigned to you.\n")
                        m = 0
                    else:
                        while n:
                            print("List of Courses and CRNs:")
                            for r in results:
                                print(r)
                            tempnme = input("Enter the name of the class you would like to view the roster of.\n")
                            cursor.execute("SELECT COURSE_NAME FROM COURSES WHERE PROFESSOR = ?", (tempname,))
                            results2 = cursor.fetchall()
                            if len(results2) == 0:
                                print("That is an invalid Name, or it is not registered to you, please try again.\n")
                            else:
                                print("Email Addresses of currenty registered students:\n")
                                eval(tempnme).printroster()
                                m = 0
                                n = 0
                                k = 1
        elif ins == '1':
            print("Logging out\n")
            k = 0
        else:
            print("Invalid entry.\n")