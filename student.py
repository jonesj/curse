import sqlite3
from instructor import *
database = sqlite3.connect("curse.db")
cursor = database.cursor()


class schedule:
    sched = [0] * 8  # capacity of 8 classes per student

    def addclass(self, id):
        self.sched.append(id)

    def removeclass(self, id):
        for i in range(len(self.sched)):
            if self.sched[i] == id:
                del self.sched[i]

    def printsched(self):
        print(" Courses currently registered for.")
        for i in range(len(self.sched)):
            if self.sched[i] == 0:
                continue
            else:
                temp = self.sched[i]
                cursor.execute("SELECT CRN, COURSE_NAME, TIME, DAYS, PROFESSOR FROM COURSES WHERE CRN = ?", (temp,))
                res = cursor.fetchall()
                for j in res:
                    print(j)

# empty class schedules for the students in the system
newtoni = schedule()
curiem = schedule()
teslan = schedule()
edistont = schedule()
vonneumannj = schedule()
hopperg = schedule()
jemisonm = schedule()
deanm = schedule()
faradaym = schedule()
lovelacea = schedule()


def student(stu):
    user = stu
    k = 1
    j = 1
    while k:
        sell = input(" 0: Fall 2018\n 1: Spring 2019\n")  # semester selection
        if sell == '0':
            sem = 'Fall'
            j = 1
        elif sell == '1':
            sem = 'Spring'
            j = 1
        else:
            print("Invalid semester\n")
            j = 0
            k = 1
        while j:
            stu = input("0: Print Schedule \n1: Register for Course \n2: Logout \n3: Select new semester\n")
            if stu == '0':
                eval(user).printsched()
            elif stu == '1':
                selec = input(" 0: Search courses by title\n 1: Search courses by Time\n 2: Search courses by Profesor\n 3: Display all courses\n")
                if selec == '0':
                    nme = input("What is the course title\n")
                    cursor.execute("SELECT * FROM COURSES WHERE COURSE_NAME = ? AND SEMESTER = ?", (nme, sem,))
                    results = cursor.fetchall()
                    if len(results) == 0:   # checking to see if course exists in the chosen semester
                        print("Course not found.")
                        k = 1
                        j = 1
                    else:
                        for i in results:
                            print(i)
                            sl = input("Enter the course ID if you would like to register for that class, otherwise enter N.\n")
                            if sl == 'N' or sl == 'n':
                                k = 1
                                j = 1
                            else:
                                cursor.execute("SELECT * FROM STUDENT WHERE EMAIL = ? AND HAS_HOLD = 'yes'", (user,))
                                res = cursor.fetchall()
                                if len(res) == 0:
                                    eval(user).addclass(sl)
                                    cursor.execute('SELECT COURSE_NAME FROM COURSES WHERE CRN = ?', (sl,))
                                    for row in cursor.fetchall():
                                        cls = row[0]
                                    eval(cls).addstudent(user)
                                else:
                                    print("You have a hold on your account and are unable to register for courses.\n")
                elif selec == '1':
                    tme = input("What time\n")
                    cursor.execute("SELECT * FROM COURSES WHERE TIME = ? AND SEMESTER = ?", (tme, sem,))
                    results = cursor.fetchall()
                    if len(results) == 0:
                        print("No courses at that time")
                        k = 1
                    else:
                        for i in results:
                            print(i)
                            sl = input("Enter the course ID if you would like to register for that class, otherwise enter N.\n")
                            if sl == 'N' or sl == 'n':
                                k = 1
                                j = 1
                            else:
                                cursor.execute("SELECT * FROM STUDENT WHERE EMAIL = ? AND HAS_HOLD = 'yes'", (user,))
                                res = cursor.fetchall()
                                if len(res) == 0:
                                    eval(user).addclass(sl)
                                    cursor.execute('SELECT COURSE_NAME FROM COURSES WHERE CRN = ?', (sl,))
                                    for row in cursor.fetchall():
                                        cls = row[0]
                                    eval(cls).addstudent(user)
                                else:
                                    print("You have a hold on your account and are unable to register for courses.\n")
                elif selec == '2':
                    prf = input("What is the professor's last name\n")
                    cursor.execute("SELECT * FROM COURSES WHERE PROFESSOR = ? AND SEMESTER = ?", (prf, sem,))
                    results = cursor.fetchall()
                    if len(results) == 0:
                        print("That Professor is not teaching any courses.")
                        k = 1
                        j = 1
                    else:
                        for i in results:
                            print(i)
                            sl = input("Enter the course ID if you would like to register for that class, otherwise enter N.\n")
                            if sl == 'N' or sl == 'n':
                                k = 1
                                j = 1
                            else:
                                cursor.execute("SELECT * FROM STUDENT WHERE EMAIL = ? AND HAS_HOLD = 'yes'", (user,))
                                res = cursor.fetchall()
                                if len(res) == 0:
                                    eval(user).addclass(sl)
                                    cursor.execute('SELECT COURSE_NAME FROM COURSES WHERE CRN = ?', (sl,))
                                    for row in cursor.fetchall():
                                        cls = row[0]
                                    eval(cls).addstudent(user)
                                else:
                                    print("You have a hold on your account and are unable to register for courses.\n")
                elif selec == '3':
                    cursor.execute('SELECT * FROM COURSES WHERE SEMESTER = ?', (sem,))
                    results = cursor.fetchall()
                    for i in results:
                        print(i)
            elif stu == '2':
                print("Logging Out\n")
                k = 0
                j = 0
            elif stu == '3':
                j = 0
                k = 1
            else:
                print("Invalid Entry")