import sqlite3
import admin as adm
import instructor as ins
import student as st
database = sqlite3.connect("curse.db")
cursor = database.cursor()

i = 1
while i:
        tempuser = input("Enter your username \n")
        cursor.execute(" SELECT NAME, SURNAME FROM ADMIN WHERE EMAIL = ?", (tempuser,))
        results = cursor.fetchall()
        if len(results) == 1:
            temppassword = input("Hello Admin, enter your password\n")
            cursor.execute("SELECT NAME,SURNAME FROM ADMIN WHERE PASSWORD = ?", (temppassword,))
            results = cursor.fetchall()
            if len(results) == 1:
                adm.admin()
            else:
                print("Invalid password.")
                i = 1
        elif len(results) == 0:
            cursor.execute(" SELECT NAME, SURNAME FROM INSTRUCTOR WHERE EMAIL = ?", (tempuser,))
            results = cursor.fetchall()
            if len(results) == 1:
                temppassword = input("Hello Instructor, enter your password\n")
                cursor.execute("SELECT NAME,SURNAME FROM INSTRUCTOR WHERE PASSWORD = ?", (temppassword,))
                results = cursor.fetchall()
                if len(results) == 1:
                    ins.instructor()
                else:
                    print("Invalid password.")
                    i = 1
            else:
                cursor.execute(" SELECT NAME, SURNAME FROM STUDENT WHERE EMAIL = ?", (tempuser,))
                results = cursor.fetchall()
                if len(results) == 1:
                    temppassword = input("Hello Student, enter your password\n")
                    cursor.execute("SELECT NAME,SURNAME FROM STUDENT WHERE PASSWORD = ?", (temppassword,))
                    results = cursor.fetchall()
                    if len(results) == 1:
                        st.student(tempuser)
                    else:
                        print("Invalid password.")
                        i = 1
        else:
            print("Invalid username.")
            i = 1