import sqlite3

database = sqlite3.connect("curse.db")
cursor = database.cursor()


def admin():
    k = 1
    while k:
        adm = input("Welcome Admin. What would you like to do?\n0: Add user\n1: Remove user\n2: Edit admin title\n3: Queried Courses\n4: Display all\n5:Add Course\n6: Remove Course\nq: Logout\n")
        if adm == '0':
            tpe = input("a: New Admin\n i: New Instructor\n s: New Student\n")
            if tpe == 'a':
                fn = input("First Name:\n")
                ln = input("Last Name:\n")
                id = input("Employee ID:\n")
                ttl = input("Title:\n")
                eml = input("Email:\n")
                off = input("Office:\n")
                pw = input("Password:\n")
                cursor.execute("INSERT INTO ADMIN VALUES(?, ?, ?, ?, ?, ?, ?)", (id, fn, ln, ttl, off, eml, pw,))
            elif tpe == 'i':
                fn = input("First Name:\n")
                ln = input("Last Name:\n")
                id = input("Employee ID:\n")
                ttl = input("Title:\n")
                yh = input("Year Hired:\n")
                dp = input("Department:\n")
                eml = input("Email:\n")
                pw = input("Password:\n")
                cursor.execute("INSERT INTO INSTRUCTOR VALUES(?, ?, ?, ?, ?, ?, ?, ?)", (id, fn, ln, ttl, yh, dp, eml, pw,))
            elif tpe == 's':
                fn = input("First Name:\n")
                ln = input("Last Name:\n")
                id = input("Employee ID:\n")
                gy = input("Graduation Year:\n")
                eml = input("Email:\n")
                mjr = input("Major:\n")
                pw = input("Password:\n")
                cursor.execute("INSERT INTO STUDENT VALUES(?, ?, ?, ?, ?, ?, ?)", (id, fn, ln, gy, mjr, eml, pw,))
                cursor.execute("SELECT * FROM STUDENT")
                results = cursor.fetchall()
                for i in results:
                    print(i)
            else:
                print("Invalid entry")
        elif adm == '1':
            rmu = input("a: Remove Admin\n i: Remove Instructor\n s: Remove Student\n")
            if rmu == 'a':
                rm = input("Enter the ID of the Admin you wish to remove.\n")
                cursor.execute("DELETE FROM ADMIN WHERE ID = ?", (rm,))
            elif rmu == 'i':
                cursor.execute("SELECT * FROM INSTRUCTOR")
                results = cursor.fetchall()
                for i in results:
                    print(i)
                rm = input("Enter the ID of the Instructor you wish to remove.\n")
                cursor.execute("DELETE FROM INSTRUCTOR WHERE ID = ?", (rm,))
                cursor.execute("SELECT * FROM INSTRUCTOR")
                results = cursor.fetchall()
                for i in results:
                    print(i)
            elif rmu == 's':
                rm = input("Enter the ID of the Student you wish to remove.\n")
                cursor.execute("DELETE FROM STUDENT WHERE ID = ?", (rm,))
            else:
                print("Invalid entry.")
        elif adm == '2':
            cursor.execute("SELECT * FROM ADMIN WHERE TITLE = 'President'")
            query_result = cursor.fetchall()
            for i in query_result:
                print(i)
            idd = input("Enter the ID of the user you wish to edit.\n")
            titl = input("Enter the new title for the user.\n")
            cursor.execute("UPDATE ADMIN set TITLE = ? WHERE ID = ?", (titl, idd,))
            cursor.execute("SELECT * FROM ADMIN WHERE NAME = 'Barack'")
            query_result = cursor.fetchall()
            for i in query_result:
                print(i)
        elif adm == '3':
            dep = input("What department would you like to query?\n")
            print("Course Querying for available professors in", dep)
            cursor.execute("SELECT  COURSE_NAME, NAME, SURNAME FROM INSTRUCTOR, COURSES WHERE COURSES.DEPARTMENT = ? AND INSTRUCTOR.DEPT == COURSES.DEPARTMENT ", (dep,))
            results = cursor.fetchall()
            if len(results) == 0:
                print("Invalid department")
            else:
                for i in results:
                    print(i)
        elif adm == '4':
            print("\nStudent Information\n")
            cursor.execute("SELECT * FROM STUDENT")
            results = cursor.fetchall()
            for i in results:
                print(i)
            print("\n Instructor Information\n")
            cursor.execute("SELECT * FROM INSTRUCTOR")
            results = cursor.fetchall()
            for i in results:
                print(i)
            print("\nAdmin Information\n")
            cursor.execute("SELECT * FROM ADMIN")
            results = cursor.fetchall()
            for i in results:
                print(i)
            print("\nCourse Information\n")
            cursor.execute("SELECT * FROM COURSES")
            results = cursor.fetchall()
            for i in results:
                print(i)
        elif adm == '5':
            newCRN = input("What is the CRN of the course you want to add?\n")
            newCourse = input("What is the name of the course you want to add?\n")
            newDep = input("What is the department of the course you want to add?\n")
            newProf = input("What is the professor of the course you want to add?\n")
            newTime = input("What is the time of the course you want to add?\n")
            isPreReq = input("Is there a prerequisite?\ny: yes\nn: no\n")
            if isPreReq == 'y':
                newPrereq = input("What is the prerequisite of the course you want to add?\n")
            else:
                newPrereq = None
            newDay = input("What are the days of the course you want to add?\n")
            newSem = input("Which semester is the course you want to add in?\n")
            newYear = input("Which year is the course you want to add in?\n")
            newCredit = input("How many credits does the course you want to add have??\n")
            if isPreReq == 'y':
                cursor.execute("INSERT INTO COURSES VALUES(?, ?, ?, ?, ?, ?, ?, ? , ?, ?)", (newCRN, newCourse, newDep, newProf, newTime, newPrereq, newDay, newSem, newYear, newCredit))
            else: 
                cursor.execute("INSERT INTO COURSES VALUES(?, ?, ?, ?, ?, ?, ?, ? , ?, ?)", (newCRN, newCourse, newDep, newProf, newTime, "NULL", newDay, newSem, newYear, newCredit))
        elif adm == '6':
            rmCRN = input("What is the CRN of the course you want to remove?\n")
            cursor.execute("SELECT * FROM COURSES")
            results = cursor.fetchall()
            cursor.execute("DELETE FROM COURSES WHERE CRN = ?", (rmCRN,))

        elif adm == 'q':
            print("Logging Out\n")
            k = 0
        else:
            print("Invalid entry.")
