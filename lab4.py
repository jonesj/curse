#Python
from array import *

class user:
    first_name = ''
    last_name = ''
    def setname(self, fn, ln):
        self.first_name = fn
        self.last_name = ln

    #insructor and student are derived from the base user class
class instructor(user):
        username=0
        password=0
        roster=["test","sample","example"]#As this is a test of basic functions, the class roster is simplified. In the full version it will be more complex, being adjusted by students registering
        crn=0
        def setpassword(self,p):
                self.password=p
        def setusername(self,u):
                self.username=u
        def setcrn(self,c):
                self.crn=c
        def getpassword(self):
                return self.password
        def getusername(self):
                return self.username
        def getroster(self,i):#As the roster is an array, this works with a loop to iterate through the entire array
                return self.roster[i]
        def getcrn(self):
                return self.crn

class student(user):
    username = 0
    password = 0
    courses = ['biology','Physics','Programming','Art']
    def setpassword(self, p):
        self.password = p

    def setusername(self, u):
        self.username = u

    def getpassword(self):
        return self.password

    def getusername(self):
        return self.username
    def getcourses(self,i):
        return self.courses[i]


student1 = student()
student1.setusername("temp")
student1.setpassword("password")
student1.setname('Test', 'Subject')


instructor1=instructor()
instructor2=instructor()

instructor1.setname("Teacher","One")
instructor2.setname("Teacher","Two")
instructor1.setusername("username")#For the sake of testing functionality, all values are hardcoded to test the process
instructor2.setusername("password")
instructor1.setpassword("hello")
instructor2.setpassword("world")
instructor1.setcrn("12345")#Again for testing, only one CRN is assigned to each Instructor. In the full version more will be added
instructor2.setcrn("54321")


i=0#These numbers are used to create loops later on, so the values are just initialized here for now
j=0
k=0
m=0#The letter l looked like the number 1, so m was used to avoid confusion
while i<1:#While loops are used, so that the user inputs again if an invalid response is given
        templogin=input("Enter your login username\nInstructor 1: username\nInstructor 2: password\n")
        if templogin==instructor1.getusername():#Checking if the entered username matches the instructor's
                temppassword=input("Welcome Instructor 1. Please enter your password.\n")
                if temppassword==instructor1.getpassword():#Same procedure for password
                        while(j<1):
                                temp=input("What would you like to do Instructor 1?\nEnter 1 to check class roster\nEnter 2 to logout\n")
                                if temp=="1":
                                        while(k<1):
                                                tempcrn=input("Enter the CRN of the course you would like to view.\nYour assigned courses are: 12345\n")
                                                if tempcrn==instructor1.getcrn():
                                                    while(m<3):#Looping through the array, calling on instructor 1's roster
                                                                print(instructor1.getroster(m))
                                                                print("\n")
                                                                m=m+1
                                                                k=1
                                                else:
                                                        print("Sorry, that is not a valid CRN, please enter another one")
                                        j=1
                                elif temp=="2":
                                        print("Logging out now")
                                        j=1
                                else:
                                        print("Sorry, that is not a valid selection.")
                        i=1
                else:
                        print("Sorry, that password is incorrect, please enter your username again\n")

        elif templogin==instructor2.getusername():#Same procedure as above, now for when Instructor 2's username is entered.
                temppassword=input("Welcome Instructor 2. Please enter your password.\n")
                if temppassword==instructor2.getpassword():
                        while(j<1):
                                temp=input("What would you like to do Instructor 2?\nEnter 1 to check class roster\nEnter 2 to logout\n")
                                if temp=="1":
                                        while(k<1):
                                                tempcrn=input("Enter the CRN of the course you would like to view.\nYour assigned courses are: 12345\n")
                                                if tempcrn==instructor1.getcrn():
                                                        while(m<3):
                                                                print(instructor1.getroster(m))
                                                                print("\n")
                                                                m=m+1
                                                        k=1
                                                else:
                                                        print("Sorry, that is not a valid CRN, please enter another one")
                                        j=1
                                elif temp=="2":
                                        print("Logging out now")
                                        j=1
                                else:
                                        print("Sorry, that is not a valid selection.")
                        i=1
                else:
                        print("Sorry, that password is incorrect, please enter your username again\n")
        elif templogin==student1.getusername(): # if the user enters a student username
            temppassword = input("Welcome Student, Enter your password :")
            if temppassword== student1.getpassword():
                while j < 1:
                    temp = input("Enter 1 to check you class schedule\nEnter 2 to register for another class\nEnter 3 to logout")
                    if temp =='1': #checking class schedule
                        print("Currently enrolled in:")
                        while m < 6:
                            print(student1.getcourses(m))
                            m = m+1
                    elif temp == '2' :
                        cls = input("What is the name of the class you wish to register for? ")
                        student1.courses.append(cls)
                    elif temp == '3' :
                        print("Logging out now")
                        j = 1
                    else:
                        print("Invalid entry, try again")
        else:
                print("Sorry, that is not a valid username. Please try again.\n")
