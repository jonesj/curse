import sqlite3

database = sqlite3.connect("curse.db")
cursor = database.cursor()

# create and populate tables

sql_command = """CREATE TABLE STUDENT (
ID 		    INT 	PRIMARY KEY 	NOT NULL,
NAME		TEXT	NOT NULL,
SURNAME		TEXT 	NOT NULL,
GRADYEAR    INT     NOT NULL,
MAJOR		CHAR(4) NOT NULL,
EMAIL		TEXT	NOT NULL,
PASSWORD    TEXT    NOT NULL,
HAS_HOLD    TEXT,
IS_ATHLETE  TEXT)

;"""
cursor.execute(sql_command)

sql_command = """CREATE TABLE INSTRUCTOR (
ID 		    INT 	PRIMARY KEY 	NOT NULL,
NAME		TEXT	NOT NULL,
SURNAME		TEXT 	NOT NULL,
TITLE		TEXT 	NOT NULL,
HIREYEAR    INT     NOT NULL,
DEPT		CHAR(4) NOT NULL,
EMAIL		TEXT	NOT NULL,
PASSWORD    TEXT    NOT NULL)
;"""
cursor.execute(sql_command)

sql_command = """CREATE TABLE ADMIN (
ID 		    INT 	PRIMARY KEY 	NOT NULL,
NAME		TEXT	NOT NULL,
SURNAME		TEXT 	NOT NULL,
TITLE		TEXT 	NOT NULL,
OFFICE		TEXT 	NOT NULL,
EMAIL		TEXT	NOT NULL,
PASSWORD    TEXT    NOT NULL)
;"""
cursor.execute(sql_command)

sql_command = """CREATE TABLE COURSES (
CRN		        INT 	PRIMARY KEY 	NOT NULL,
COURSE_NAME	    TEXT	NOT NULL,
DEPARTMENT 	    TEXT 	NOT NULL,
PROFESSOR	    TEXT 	NOT NULL,
TIME		    INT 	NOT NULL,
PREREQUISITE    TEXT,
DAYS            TEXT    NOT NULL,
SEMESTER        TEXT    NOT NULL,
YEAR            INT     NOT NULL,
CREDITS         INT     NOT NULL)
;"""
cursor.execute(sql_command)

#create courses
cursor.execute("INSERT INTO COURSES VALUES(11199, 'Digital_Logic', 'BSEE', 'Fourier', 10,'Differential Equations', 'Monday', 'Fall', 2018, 4);")
cursor.execute("INSERT INTO COURSES VALUES(11200, 'Differential_Equations', 'BSME', 'Bernoulli', 3, NULL, 'Tuesday', 'Spring', 2019, 4);")
cursor.execute("INSERT INTO COURSES VALUES(11201, 'Circuits', 'BSEE', 'Fourier', 8, NULL, 'Friday', 'Fall', 2018, 4);")
cursor.execute("INSERT INTO COURSES VALUES(11202, 'English', 'HUSS', 'Mandela', 5,  NULL, 'Wednesday', 'Fall', 2018, 4);")
cursor.execute("INSERT INTO COURSES VALUES(11203, 'Digital_Circuit_Design', 'BSCO', 'Bouman', 1, 'Circuits', 'Monday', 'Spring', 2019, 4);")
cursor.execute("INSERT INTO COURSES VALUES(11204, 'Nanotechnology', 'BSCO', 'Turing', 3,'Digital Circuit Design', 'Friday', 'Spring', 2019, 4);")

cursor.execute("""INSERT INTO STUDENT VALUES(00010001, 'Isaac', 'Newton', 1668, 'BSAS', 'newtoni', 'isaacn', NULL, NULL);""")
cursor.execute("""INSERT INTO STUDENT VALUES(00010002, 'Marie', 'Curie', 1903, 'BSAS', 'curiem', 'mariec', NULL, NULL);""")
cursor.execute("""INSERT INTO STUDENT VALUES(00010003, 'Nikola', 'Tesla', 1878, 'BSEE', 'teslan', 'nikolat', 'yes', NULL);""")
cursor.execute("""INSERT INTO STUDENT VALUES(00010004, 'Thomas', 'Edison', 1879, 'BSEE', 'edisont', 'thomase', NULL, 'yes');""")
cursor.execute("""INSERT INTO STUDENT VALUES(00010005, 'John', 'von Neumann', 1923, 'BSCO', 'vonneumannj', 'johnv', NULL, NULL);""")
cursor.execute("""INSERT INTO STUDENT VALUES(00010006, 'Grace', 'Hopper', 1928, 'BCOS', 'hopperg', 'graceh', NULL, NULL);""")
cursor.execute("""INSERT INTO STUDENT VALUES(00010007, 'Mae', 'Jemison', 1981, 'BSCH', 'jemisonm', 'maej', NULL, NULL);""")
cursor.execute("""INSERT INTO STUDENT VALUES(00010008, 'Mark', 'Dean', 1979, 'BSCO', 'deanm', 'markd', NULL, NULL);""")
cursor.execute("""INSERT INTO STUDENT VALUES(00010009, 'Michael', 'Faraday', 1812, 'BSAS', 'faradaym', 'michaelf', NULL, NULL);""")
cursor.execute("""INSERT INTO STUDENT VALUES(00010010, 'Ada', 'Lovelace', 1832, 'BCOS', 'lovelacea', 'adal', NULL, NULL);""")

cursor.execute("""INSERT INTO INSTRUCTOR VALUES(00020001, 'Joseph', 'Fourier', 'Full Prof.', 1820, 'BSEE', 'fourierj', 'josephf');""")
cursor.execute("""INSERT INTO INSTRUCTOR VALUES(00020002, 'Nelson', 'Mandela', 'Full Prof.', 1994, 'HUSS', 'mandelan', 'nelsonm');""")
cursor.execute("""INSERT INTO INSTRUCTOR VALUES(00020003, 'Galileo', 'Galilei', 'Full Prof.', 1600, 'BSAS', 'galileig', 'galileog');""")
cursor.execute("""INSERT INTO INSTRUCTOR VALUES(00020004, 'Alan', 'Turing', 'Associate Prof.', 1940, 'BSCO', 'turinga', 'alant');""")
cursor.execute("""INSERT INTO INSTRUCTOR VALUES(00020005, 'Katie', 'Bouman', 'Assistant Prof.', 2019, 'BSCO', 'boumank', 'katieb');""")
cursor.execute("""INSERT INTO INSTRUCTOR VALUES(00020006, 'Daniel', 'Bernoulli', 'Associate Prof.', 1760, 'BSME', 'bernoullid', 'danielb');""")

cursor.execute("""INSERT INTO ADMIN VALUES(00030001, 'Barack', 'Obama', 'President', 'Dobbs 1600', 'obamab', 'baracko');""")
cursor.execute("""INSERT INTO ADMIN VALUES(00030002, 'Malala', 'Yousafzai', 'Registrar', 'Wentworth 101', 'yousafzaim', 'malalay');""")

database.commit()
database.close()